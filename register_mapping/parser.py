#!/usr/bin/python3
# ----------------
# Relevant HTML file that is parsed here to construct the json object
# can be downloaded from:
#
# https://files.sma.de/downloads/PARAMETER-HTML_SBxx-1AV-41-V15.zip?_ga=2.98077407.1347438858.1714741927-892053633.1714741927
#
# ----------------
from bs4 import BeautifulSoup
import json


def convert_list_to_dict(data: list, header: list[str], name_key: str) -> dict:
	dictionary: dict = {}

	for row in data:
		if len(row) < 1:
			continue
		val = {header[i]: row[i] for i in range(len(row)-1)}
		key = val[name_key]
		dictionary[key] = val
		
	return dictionary


def main():
	# Parse the first file
	# --------------------
	with open("./parameterlist_en.html", "r") as fb:
		data = fb.read()
	soup = BeautifulSoup(data, 'html.parser')

	header = ["Channel",
		   "Object Type",
		   "Name (SMA Speedwire)",
		   "Read Level",
		   "Group",
		   "Value Range / Unit / Set values",
		   "Default value",
		   "Step Size",
		   "Write Level",
		   "Grid Guard",
		   "SMA Modbus Register Adress",
		   "Number of contiguous SMA Modbus Registers",
		   "SMA Modbus Data Type",
		   "SMA Modbus Data Format",
		   "SMA Modbus Access",]


	table = soup.find("table")
	table_data = []
	table_data.append(header) 

	if table: # Extract the table from the html
		rows = table.find_all('tr') # type: ignore # Find all rows in the table
		for row in rows:
			cells = row.find_all('td')  # Find all cells in each row
			row_data = []
			for cell in cells:
				row_data.append(cell.text)
			table_data.append(row_data)
	table_data.pop(0)
	data_dict = convert_list_to_dict(table_data, header, "SMA Modbus Register Adress")

	# Write to a file
	data_dict_json = json.dumps(data_dict, indent=4)
	with open("registers.json", "w") as fb:
		fb.write(data_dict_json)

	name = "Name (SMA Speedwire)"
	measurement_names = []
	for _, value in data_dict.items():
		if value["Object Type"] == "Measured value":
			if value[name] not in measurement_names:
				measurement_names.append(value[name])

	# Parse the old file
	# ------------------
	with open("modbuslist_en_old.html", "r") as fb:
		data = fb.read()
	soup = BeautifulSoup(data, 'html.parser')

	header = ["Register adresse SMA",
			  "Number of contiguous SMA registers",
			  "Data type SMA",
			  "Data format SMA",
			  "Access SMA",
			  "Register adresse SunSpec",
			  "Name SunSpec",
			  "Information model SunSpec",
			  "Number of contiguous SunSpec registers",
			  "Data type SunSpec",
			  "Scaling faktor register SunSpec",
			  "Access SunSpec",
			  "Unit",
			  "Value range",
			  "Default value",
			  "Grid Guard",
			  "Device Control Object",
			  "Parameter name for Speedwire/Webconnect and WLAN",
			  "Parameter name for RS485",
			  "Objekt name SMA",]

	table = soup.find("table")
	table_data = []
	table_data.append(header) 

	if table: # Extract the table from the html
		rows = table.find_all('tr') # type: ignore # Find all rows in the table
		for row in rows:
			cells = row.find_all('td')  # Find all cells in each row
			row_data = []
			for cell in cells:
				row_data.append(cell.text)
			table_data.append(row_data)
	table_data.pop(0)
	data_dict = convert_list_to_dict(table_data, header, "Register adresse SMA")

	# Write to a file
	data_dict_json = json.dumps(data_dict, indent=4)
	with open("registers_old.json", "w") as fb:
		fb.write(data_dict_json)


if __name__ == "__main__":
	main()
