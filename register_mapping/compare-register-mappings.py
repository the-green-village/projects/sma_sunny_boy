#!/usr/bin/python3

import json
from measurements_list import UNUSED_REGISTERS, NON_MEASUREMENT_REGISTERS

def print_table(table: list[list[str]]) -> None:
    """
    Utility function for print a table from a list of a list of strings
    """
    longest_cols = [
        (max([len(str(row[i])) for row in table]) + 3)
        for i in range(len(table[0]))
    ]
    row_format = " |".join(["{:>" + str(longest_col) + "}" for longest_col in longest_cols])
    for row in table:
        print(row_format.format(*row))



registers_old_filename = "registers_old.json"
registers_new_filename = "registers.json"


# Load in old and new register data
with open(registers_old_filename, 'r') as fh:
    registers_old = json.loads(fh.read())

with open(registers_new_filename, 'r') as fh:
    registers = json.loads(fh.read())


matched_keys = []
only_in_new = []
only_in_old = []

registers_old_keys = registers_old.keys()
registers_new_keys = registers.keys()

for key, value in registers.items():
    if key in registers_old_keys:
        matched_keys.append(key)
    else:
        only_in_new.append(key)

for key, value in registers_old.items():
    if key not in (matched_keys + only_in_new):
        only_in_old.append(key)

# filter the keys in new to only keep used keys and remove anything that's not a measured value
pre = len(only_in_new)
only_in_new = list(
        filter(
            lambda key: registers[key]["Object Type"] == "Measured value",
            filter(lambda key: key not in UNUSED_REGISTERS, only_in_new)
            )
        )
# print(f"{pre-len(only_in_new)} items filtered")

for key in UNUSED_REGISTERS:
    registers.pop(str(key), None)

for key in NON_MEASUREMENT_REGISTERS:
    registers.pop(str(key), None)


for key in registers.keys():
    if registers[key]["Object Type"] != "Measured value":
        pass
    if key in matched_keys:
        registers[key]["Unit"] = registers_old[key]["Unit"]
    else:
        registers[key]["Unit"] = "-"

with open("filtered_registers.json", 'w') as fh:
    fh.write(json.dumps(registers, indent=4))
