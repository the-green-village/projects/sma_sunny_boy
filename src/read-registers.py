#!/usr/bin/python3
import json
from pymodbus.client import ModbusTcpClient
from pymodbus.exceptions import ModbusIOException

# Relative imports
from projectsecrets import MODBUS_HOST, MODBUS_PORT, TOPIC

def construct_register_value(reg: list[int], data_type) -> int | None:
    """
    Take a list of register values read from the modbus connection and reconstruct
    the integer value corresponding to the read values.

    Modbus registers are formatted as 
    - reg[0] is the most significant word
    - reg[n] is the least significant word

    let `ws = 16` be the modbus word size. It follows that the integer value can be computed using
    ```
    result = reg[0] << (n-1)*ws | reg[1] << (n-2)*ws | ... | reg[n]
    ```

    Parameters
    ----------
    `reg: list[int]`, the list of 16bit values read from the Modbus registers

    Returns
    -------
    `result: int | None`, result of reconstructing the read value. Returns None if an empty list is given
    """
    n = len(reg)
    if n == 0:
        return None # Return None if the list is empty

    result = 0
    word_size = 16
    for i in range(0, n):
        result = reg[i] << (n-(i+1))*word_size | result

    if data_type in ["S16", "S32"]:
        bits = word_size*n # Number of bits 
        sign = ((1 << bits-1) & result) # Extract sign bit

        # Construct bitmask for constructing signed integer
        bitmask = 0xFFFF
        for i in range(n):
            bitmask = (bitmask << i*word_size) | 0xFFFF
        # Reconstruct signed integer
        if sign == 0:
            result &= bitmask
        else: 
            result = -1*(result & bitmask)
    return result

nan_values = {
        "S16": 0x8000,
        "S32": 0x8000_0000,
        "STR32": 0x5A45524F, # UTF-8 encoding for 'ZERO'
        "U16": 0xFFFF,
        "U32": 0xFFFF_FFFF,
        "U64": 0xFFFF_FFFF_FFFF_FFFF,
        }

client = ModbusTcpClient(MODBUS_HOST, int(MODBUS_PORT))
client.connect()

with open('../register_mapping/filtered_registers.json', 'r') as fh:
    registers = json.loads(fh.read())

for reg, val in registers.items():
    reg = int(reg)
    contiguous = int(val["Number of contiguous SMA Modbus Registers"])
    data_type = val["SMA Modbus Data Type"]
    try:
        scaling_factor = float(val["Step Size"])
    except Exception:
        continue
    name = val["Name (SMA Speedwire)"]
    unit = val["Unit"]
    nan = nan_values[data_type]

    res = client.read_holding_registers(reg, count=contiguous, slave=3)

    if type(res) == ModbusIOException:
        # Explicitly handle the error case
        print(res.message)
        exit(1)

    # Otherwise just extract the value
    read_value_raw = construct_register_value(res.registers, data_type)

    if read_value_raw is not None and read_value_raw != nan:
        read_data = scaling_factor*read_value_raw
        if unit != "-":
            print(f"{reg} - {name: <40} | [{data_type}]: {res.registers}, {hex(read_value_raw)}, {read_data} {unit}")
        else:
            print(f"{reg} - {name: <40} | [{data_type}]: {res.registers}, {hex(read_value_raw)}, {read_data} !!!")



