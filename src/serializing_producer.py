#!/usr/bin/python3
import json
import yaml
import logging
import time
from datetime import datetime as dt
from pymodbus.client import ModbusTcpClient
from pymodbus.exceptions import ModbusIOException
from tgvfunctions import tgvfunctions

# Relative imports
import utils
from projectsecrets import MODBUS_HOST, MODBUS_PORT, TOPIC


# Setting up the logger
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.WARNING
)
logger = logging.getLogger(__file__)


# The docs for the SMA sunnyboy list some specific NaN values for the different data types
# this dict is used for checking and verifying measurements
NAN_VALUES = {
        "S16": 0x8000,
        "S32": 0x8000_0000,
        "STR32": 0x5A45524F, # UTF-8 encoding for 'ZERO'
        "U16": 0xFFFF,
        "U32": 0xFFFF_FFFF,
        "U64": 0xFFFF_FFFF_FFFF_FFFF,
        }

# The following is the list of measurements actually pushed with the name 
# specified as listed in the modbus documentation from SMA
SELECTED_MEASUREMENTS = [
        "Daily yield",
        "Total yield",
        "DC current input",
        "DC current input",
        "DC voltage input",
        "DC voltage input",
        "DC power input",
        "DC power input",
        "Grid frequency",
        "Grid current phase L1",
        "Grid voltage phase L1",
        "Power L1",
        "Apparent power L1",
        "Reactive power L1",
        "Displacement power factor",
        "Residual current",
        "Internal temperature",
        # The following measurements do not return a NaN when read but will be ignored
        # ----------------------------------------------------------------------------
        # "Remaining duration until CP restart",
        # "Number of events for installer",
        # "Number of events for service",
        # "Number of events for user",
        # "Nominal power in Fault Mode",
        # "Nominal power in Ok Mode",
        # "Nominal power in Warning Mode",
        # "EEI displacement power factor",
        # "Residual current",
        # "Insulation resistance",
        # "Operating time",
        # "Feed-in time",
        # "Number of grid connections",
        # "Current spec. active power limitation P ",
        # "Intermediate circuit voltage",
        # "Internal temperature",
        ]

class ConnectionException(Exception):
    pass

class InvalidLengthException(Exception):
    pass

class MissingRequiredArgumentException(Exception):
    pass


def get_measurement_string(reg: str) -> str:
    if reg in ["30769", "30771", "30773"]:
        return "A"
    else:
        return "B"
    

def map_measurement_name(sma_name: str, meas_string = None) -> str:
    """
    Take a sma modbus string name and map this to the name for the kafka message

    Parameters
    ----------
    `sma_name: str`, name in the SMA documentation
    `meas_string: str | None = None`, DC measurement string, either A or B or None if N/A

    Returns
    -------
    `result_string: str`, measurement name string

    Exception
    ---------
    `MissingRequiredArgumentException`, thrown when DC measurment is passed but string is None
    `MissingRequiredArgumentException`, thrown when no or an unknown measurement is passed
    """
    result_string = ""
    match sma_name:
        case "Daily yield":
            result_string = "Daily Yield"
        case "Total yield":
            result_string = "Total Yield"
        case "DC current input":
            if meas_string is None:
                raise MissingRequiredArgumentException("Measurment String cannot be None for DC Current Input")
            result_string = f"DC Current Input - String {meas_string}"
        case "DC voltage input":
            if meas_string is None:
                raise MissingRequiredArgumentException("Measurment String cannot be None for DC Voltage Input")
            result_string = f"DC Voltage Input - String {meas_string}"
        case "DC power input":
            if meas_string is None:
                raise MissingRequiredArgumentException("Measurment String cannot be None for DC Power Input")
            result_string = f"DC Power Input - String {meas_string}"
        case "Grid frequency":
            result_string = "Grid Frequency"
        case "Grid current phase L1":
            result_string = "Grid Current Phase L1"
        case "Grid voltage phase L1":
            result_string = "Grid Voltage Phase L1N"
        case "Power L1":
            result_string = "Grid Power Phase L1"
        case "Apparent power L1":
            result_string = "Apparant Power Phase L1"
        case "Reactive power L1":
            result_string = "Reactive Power Phase L1"
        case "Displacement power factor":
            result_string = "Displacement Power Factor"
        case "Residual current":
            result_string = "Residual Current"
        case "Internal temperature":
            result_string = "Internal Temperature"
        case _:
            raise MissingRequiredArgumentException("Unknown Measurement String")

    return result_string


def construct_register_value(reg: list[int], data_type: str) -> int:
    """
    Take a list of register values read from the modbus connection and reconstruct
    the integer value corresponding to the read values.

    Modbus registers are formatted as 
    - reg[0] is the most significant word
    - reg[n] is the least significant word

    let `ws = 16` be the modbus word size. It follows that the integer value can be computed using
    ```
    result = reg[0] << (n-1)*ws | reg[1] << (n-2)*ws | ... | reg[n]
    ```

    Parameters
    ----------
    `reg: list[int]`, the list of 16bit values read from the Modbus registers

    Returns
    -------
    `result: int | None`, result of reconstructing the read value. Returns None if an empty list is given

    Exception
    ---------
    `InvalidLengthException`
    """
    n = len(reg)
    
    if n == 0:
        raise InvalidLengthException("Expected non-zero amount of items in the reg list")
    
    result = 0
    word_size = 16
    for i in range(0, n):
        result = reg[i] << (n-(i+1))*word_size | result

    if data_type in ["S16", "S32"]:
        bits = word_size*n # Number of bits 
        sign = ((1 << bits-1) & result) # Extract sign bit

        # Construct bitmask for constructing signed integer
        bitmask = 0xFFFF
        for i in range(n):
            bitmask = (bitmask << i*word_size) | 0xFFFF
        bitmask ^= (0x8000 << (n-1)*word_size)
        # Reconstruct signed integer
        if sign == 0:
            result &= bitmask
        else: 
            result = -1*(result & bitmask)
    return result


def count(input: str, arr: list) -> int:
    """
    Count how many times `input` occurs in the list `arr`

    Parameters
    ----------
    `input: str`, the string to find in `arr`
    `arr: list`, the list in which to count elements

    Returns
    -------
    `count: int`, the amount of times `input` appears in `arr`
    """
    count = 0
    for i in arr:
        if i == input:
            count += 1
        else:
            continue
    return count


def extract_selected_measurements(all_registers: dict) -> dict:
    """
    Construct a new dict with only the measurements as listed in SELECTED_MEASUREMENTS
    """
    name_key = "Name (SMA Speedwire)"
    type_key = "SMA Modbus Data Type"
    selected_registers: dict = {}
    selected_register_names: list = []
    for (key, value) in all_registers.items():
        if value[name_key] in SELECTED_MEASUREMENTS and count(value[name_key], selected_register_names) < 2:
            if value[name_key] in ["Daily yield", "Total yield"]:
                if value[type_key] == "U64":
                    selected_registers[key] = value
                    selected_register_names.append(value[name_key])
            else:
                selected_registers[key] = value
                selected_register_names.append(value[name_key])

    return selected_registers


def find_subranges(register_numbers: list[int]) -> list[tuple]:
    """
    Modbus can quickly read 256 bytes in a single batch which is faster than reading individual registers.
    This 256 bytes has 6 bytes overhead so we can read 125 contiguous registers per batch. This function
    will output the ranges to be read to efficiently retrieve all data
    """
    registers_sorted = sorted(register_numbers)
    ranges = []
    block = []
    start = int(registers_sorted[0])

    for value in registers_sorted:
        value = int(value)
        if value - start < 120:
            block.append(value)
        else:
            ranges.append(block)
            block = [value]
            start = value
    ranges.append(block) # append the final block
    return ranges


def main():
    desired_data_frequency = 1 # Messages per second
    desired_data_sampling_time = 1/desired_data_frequency

    with open("../config.yaml") as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)

    # Get the required information from the config
    project_id = config["project_id"]
    application_id = config["application_id"]
    device_id = config["device_id"]
    producer_name = config["producer_name"]

    # Get optional metadata
    device_manufacturer = config["device_manufacturer"]
    device_type = config["device_type"]

    # Define keys for easy access into the meta-data
    name_key = "Name (SMA Speedwire)"
    contiguous_key = "Number of contiguous SMA Modbus Registers"
    float_conversion_key = "Step Size"
    data_type_key = "SMA Modbus Data Type"
    unit_key = "Unit"

    # Set up modbus connection and initialise producer
    client = ModbusTcpClient(MODBUS_HOST, int(MODBUS_PORT))
    client.connect()
    tgv = tgvfunctions.TGVFunctions(TOPIC)
    producer = tgv.make_producer(producer_name)

    # Verify that connection is established
    if (client.connected == False and not client.is_socket_open()):
        raise ConnectionException("Cannot connect to modbus client")

    # Load in the registers and filter using selected measurements list
    with open('../register_mapping/filtered_registers.json', 'r') as fh:
        registers = extract_selected_measurements(json.loads(fh.read()))
    # logger.debug(json.dumps(registers, indent=2))

    
    measurements_count = len(registers.keys())
    ranges = find_subranges(list(registers.keys()))
    measurements = []
    epochms = None

    try:
        while True:
            start_time = time.time()
            start_time_measurements = time.time()
            measurements = []
            for subrange in ranges:
                start, stop = (subrange[0], subrange[-1]+4) # Add 4 extra registers to account for last registers offset
                numbers_of_registers_to_read = stop - start

                # Read the modbus registers in the range and extract the relevant ones
                epochms = dt.now().timestamp()*1000
                res = client.read_holding_registers(start, count=numbers_of_registers_to_read, slave=3)

                # Explicitly handle the error case
                if type(res) == ModbusIOException:
                    logger.warning(res.message)
                    continue
                register_data = res.registers

                # Extract relevant registers
                for reg in subrange:
                    # Get the register meta data and use it to extract all relevant info for computation
                    register_metadata = registers[str(reg)]

                    step_size = float(register_metadata[float_conversion_key])
                    data_type = register_metadata[data_type_key]
                    unit = register_metadata[unit_key]
                    name = register_metadata[name_key]
                    description = f"Register {reg} - {name}"

                    # Account for the 2 missing units for Apparent Power and Reactive Power
                    if unit == "-":
                        if name == "Apparent power L1":
                            unit = "VA"
                        elif name == "Reactive power L1":
                            unit = "var"
                        elif name == "Power L1":
                            unit = "W"

                    # Extract the relevant registers from the contiguous array
                    offset = reg - start
                    contiguous = int(register_metadata[contiguous_key])
                    data_raw = register_data[offset:offset+contiguous]
                    data = construct_register_value(data_raw, data_type)*step_size

                    if name in ["DC current input", "DC power input", "DC voltage input"]:
                        measurement_string = get_measurement_string(str(reg))
                        measurement_name = map_measurement_name(name, measurement_string)
                    else:
                        measurement_name = map_measurement_name(name)
                    measurements.append(utils.generate_measurement_dict(measurement_name, unit, data, measurement_description=description))
            measurement_construction_time = time.time() - start_time_measurements

            communication_start_time = time.time()
            msg = utils.generate_kafka_message(project_id,
                                               application_id,
                                               device_id,
                                               epochms,
                                               measurements,
                                               device_manufacturer=device_manufacturer,
                                               device_type=device_type)
            tgv.produce_fast(producer, msg)
            comm_time = time.time() - communication_start_time
            logger.info(f"""
                        Timing Info:
                            Measurement time - {measurement_construction_time} s
                            Communication time - {comm_time} s
                            Took {measurement_construction_time+comm_time} s to retrieve and push {measurements_count} measurements
                        """)
            time_delta = time.time() - start_time
            if time_delta <= desired_data_sampling_time:
                time.sleep(desired_data_sampling_time - time_delta)
    except KeyboardInterrupt:
        pass
    finally:
        logger.info(producer.flush())


if __name__ == "__main__":
    main()
    
