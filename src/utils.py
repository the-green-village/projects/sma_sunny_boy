from typing import Any, Optional

class MissingRequiredKeyException(Exception):
	pass

def generate_kafka_message(project_id: Any,
						   application_id: Any,
						   device_id: Any,
						   timestamp: Any,
						   measurements: list[dict],
						   project_description: Any = None,
						   application_description: Any = None,
						   device_description: Any = None,
						   device_manufacturer: Any = None,
						   device_type: Any = None,
						   device_serial: Any = None,
						   location_id: Any = None,
						   location_description: Any = None,
						   latitude: Any = None,
						   longitude: Any = None,
						   altitude: Any = None,) -> dict:
	"""
	Thin wrapper to generate a python dict object containing a message that can be pushed to Grafana using the serializing producer.
	"""
	# Check if all measurements have an id
	for (idx, m) in enumerate(measurements):
		if "measurement_id" not in m.keys():
			raise MissingRequiredKeyException(f"Expected measurement_id key for measurement {idx}")

	return {"project_id": project_id,
			"application_id": application_id,
			"device_id": device_id,
			"timestamp": timestamp,
			"project_description": project_description,
			"application_description": application_description,
			"device_description": device_description,
			"device_manufacturer": device_manufacturer,
			"device_type": device_type,
			"device_serial": device_serial,
			"location_id": location_id,
			"location_description": location_description,
			"latitude": latitude,
			"longitude": longitude,
			"altitude": altitude,
			"measurements": measurements,}

def generate_measurement_dict(measurement_id: str,
							  unit: str,
							  value: Any,
							  measurement_description: Optional[str] = None,) -> dict:
	"""
	Thin wrapper to generate measurement dicts.

	Detailed
	--------
	Structure for measurements is as follows:
		- `measurement_id` (Required)
		- `unit` (Required)
		- `value` (Required)
		- `measurement_description` 
	"""
	return {
			"measurement_id": measurement_id,
			"measurement_description": measurement_description,
			"unit": unit,
			"value": value, 
			}
