#!/usr/bin/bash
. ~/producers/.kafka_envs
. ~/producers/energy/sma_sunny_boy/env/bin/activate

cd ~/producers/energy/sma_sunny_boy/src

python3 async_serializing_producer.py
