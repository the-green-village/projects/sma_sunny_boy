# sma_sunny_boy modbus connection

- Firmware version `4.00.75R`


## Modbus Data Types and Dataformats
Modbus registers only support storing 16 bit values, however SB3.6-1AV-41 stores values such that fixed-point numbers and negative numbers can be retrieved from the `uint16_t` modbys register. The following describes how to interpret this bit sequence based on the information stored in the `registers.json` file.

| Type  | Description | NaN value |
| :---- | :---------- | --------: | 
| `S16` | Signed 16 bit word | `0x8000` |
| `S32` | Signed double 16 bit word (2 contiguous registers) | `0x8000 0000` |
| `STR32` | 4 UTF-8 characters | `ZERO` |
| `U16` | Unsigned 16 bit word | `0xFFFF` |
| `U32` | Unigned double 16 bit word (2 contiguous registers)¹ | `0xFFFF FFFF` |
| `U64` | Unsigned quadrupel word 16 bit word (4 contiguous registers) | `0xFFFF FFFF FFFF FFFF` |

¹ If it is a status value, only the lower 24 bit values of the word are used with NaN value `0x00FF FFFD`


The following table gives information on data formats, specifying how the read data should be interpreted.

| Format | Explanation | 
| :----- | :---------- |
| Duration | Duration in either seconds, minutes or hours, depending on the register |
| FIX0 | Decimal number with 0 decimal places |
| FIX1 | Decimal number with 1 decimal place. Multiply read value by 0.1 |
| FIX2 | Decimal number with 1 decimal place. Multiply read value by 0.01 |
| FIX3 | Decimal number with 1 decimal place. Multiply read value by 0.001 |
| FIX4 | Decimal number with 1 decimal place. Multiply read value by 0.0001 |
| TEMP | Special modbus register for temperature data that seems to be identical to FIX1? |
| IP4 | 4-byte IPv4 address of the form `XXX.XXX.XXX.XXX` |
| TM | UTC time in seconds |
| UTF8 | UTF-8 character data | 
| DT | Date-time, seconds since epoch, conforms to device country/timezone settins |

- For FIX0, FIX1, FIX2, FIX3 and FIX4 commercial rounding (i.e. rounding halves away from zero) is applied, thus:
    - `commround(0.5) = 1`
    - `commround(-0.5) = -1`



### Unused Registers
Not all registers for the SMA Sunnyboy are used. The following registers return a NaN value as documented in the table before when read

| Register | Register Name | Register | Register Name |
| :------- | :------------ | :------- | :------------ |
| 30199 | Waiting time until feed-in | 30247 | Current event number for manufacturer |
| 30581 | Counter reading of power drawn counter | 30583 | Grid feed-in counter reading |
| 30779 | Power L2 | 30781 | Power L3 |
| 30785 | Grid voltage phase L2 | 30787 | Grid voltage phase L3 |
| 30789 | Grid voltage phase L1 against L2 | 30791 | Grid voltage phase L2 against L3 |
| 30793 | Grid voltage phase L3 against L1 | 30809 | Reactive power L2 |
| 30811 | Reactive power L3 | 30817 | Apparent power L2 |
| 30819 | Apparent power L3 | 30847 | Current battery capacity |
| 30849 | Battery temperature | 30865 | Power drawn |
| 30867 | Power grid feed-in | 30979 | Grid current phase L2 |
| 30981 | Grid current phase L3 | 31159 | Current spec. reactive power Q |
| 31253 | Grid voltage phase L1 | 31255 | Grid voltage phase L2 |
| 31257 | Grid voltage phase L3 | 31259 | Power grid feeding L1 |
| 31261 | Power grid feeding L2 | 31263 | Power grid feeding L3 |
| 31265 | Power drawn from grid phase L1 | 31267 | Power drawn from grid phase L2 |
| 31269 | Power drawn from grid phase L3 | 31271 | Reactive power grid feeding phase L1 |
| 31273 | Reactive power grid feeding phase L2 | 31275 | Reactive power grid feeding phase L3 |
| 31277 | Reactive power grid feeding | 31397 | Battery charge |
| 31401 | Battery discharge | 31407 | Current spec. cos φ |
| 31411 | Current spec. reactive power Q | 31433 | Displacement power factor |
| 31435 | Grid current phase L1 | 31437 | Grid current phase L2 |
| 31439 | Grid current phase L3 | 31441 | Apparent power L1 |
| 31443 | Apparent power L2 | 31445 | Apparent power L3 |
| 31447 | Grid frequency | 31449 | Grid voltage phase L3 against L1 |
| 31451 | Grid voltage phase L1 against L2 | 31453 | Grid voltage phase L2 against L3 |
| 31455 | Apparent power | 31499 | EEI displacement power factor |
| 32201 | Current charging power | 32205 | Current discharging power |
| 32239 | End-of-charge voltage | 32245 | End-of-discharge voltage |
| 32251 | Maximum charging current | 32257 | Maximum discharging current |
| 32341 | Power grid feed-in | 34609 | Ambient temperature |
| 34611 | Highest measured ambient temperature | 34615 | Wind speed |
| 34621 | Module temperature | 34623 | Insolation on external sensor |
| 34625 | Ambient temperature | 34627 | Ambient temperature |
| 34629 | Module temperature | 34631 | Module temperature |
| 34633 | Wind speed | 34635 | Wind speed |


## Procedure for finding list of registers to read
The directory `register_mapping/` contains the relevant `html` files being parsed for retrieving the meta-data along with `python` scripts for the parsing. This will create a file called `registers.json` which is a list of all non `NaN` measured values.

### Finding Units
The project contains a file `registers.json`. This file contains the Modbus meta-data for all registers of interest. This meta-data is collected from 2 different sources, being the older version of the API documentation (Modbus® Parameters and Measured Values SB3.0-1AV-40 (9319) / SB3.6-1AV-40 (9320) / SB4.0-1AV-40 (9321) / SB5.0-1AV-40 (9322)) and the new version of the modbus interface documentation (Parameters and Measured Values SB3.0-1AV-41 / SB3.6-1AV-41 / SB4.0-1AV-41 / SB5.0-1AV-41 / SB6.0-1AV-41). There are some differences in how these 2 sources document the modbus interface, being:

- The old version lists a `Unit` field, while the new one does not
- There are various registers listed in the old documentation but not in the new one
- There are various registers listed in the new documentation but not in the old one

All registers which are in the old documentation but not in the new one were ignored. Were applicable, the Unit from the old documentation was used. 
As listed in (a previous section)[### Unused Registers], a large chunk of the registers return a `NaN` value. All of these registers were filtered.

After filtering for `NaN` values and ignoring unused registers, the remaining registers all have a designated unit or are a mirror of a register that has one. The full table for this is given below

| Address | Name | Data Type | Unit | Notes |
| :------ | :--- | :-------- | :--- | :---- |
| 30517 | Daily yield | `U64` | Wh | |
| 30535 | Daily yield | `U32` | Wh | |
| 30537 | Daily yield | `U32` | kWh | |
| 30539 | Daily yield | `U32` | MWh | |
| 30513 | Total yield | `U64` | Wh | |
| 30529 | Total yield | `U32` | Wh | |
| 30531 | Total yield | `U32` | kWh | |
| 30533 | Total yield | `U32` | MWh | |
| 32405 | Remaining duration until CP restart | `U32` | !!! | |
| 30561 | Number of events for installer | `U32` | !!! | |
| 35381 | Number of events for installer | `U64` | !!! | |
| 30563 | Number of events for service | `U32` | !!! | |
| 35385 | Number of events for service | `U64` | !!! | |
| 30559 | Number of events for user | `U32` | !!! | |
| 35377 | Number of events for user | `U64` | !!! | |
| 30207 | Nominal power in Fault Mode | `U32` | W | |
| 30203 | Nominal power in Ok Mode | `U32` | W | |
| 31085 | Nominal power in Ok Mode | `U32` | W | |
| 30205 | Nominal power in Warning Mode | `U32` | W | |
| 30769 | DC current input | `S32` | A | |
| 30957 | DC current input | `S32` | A | |
| 31793 | DC current input | `S32` | !!! | Mirror for 30769 |
| 31795 | DC current input | `S32` | !!! | Mirror for 30957 |
| 30771 | DC voltage input | `S32` | V | |
| 30959 | DC voltage input | `S32` | V | |
| 30773 | DC power input   | `S32` | W | |
| 30961 | DC power input   | `S32` | W | |
| 30803 | Grid frequency   | `U32` | Hz | |
| 30813 | Apparent power   | `S32` | VA | |
| 30805 | Reactive power   | `S32` | VAr | |
| 30775 | Power            | `S32` | W | NOT always the same as Power L1 |
| 30977 | Grid current phase L1 | `S32` | A | |
| 30783 | Grid voltage phase L1 | `U32` | V | |
| 30777 | Power L1              | `S32` | !!! | |
| 30815 | Apparent power L1     | `S32` | !!! | |
| 30807 | Reactive power L1     | `S32` | !!! | |
| 30949 | Displacement power factor | `U32` | !!! | |
| 31221 | EEI displacement power factor | `S32` | !!! | |
| 31247 | Residual current      | `S32` | A | |
| 30225 | Insulation resistance | `U32` | Ohm | |
| 30521 | Operating time        | `U64` | s | |
| 30541 | Operating time        | `U32` | s | |
| 30525 | Feed-in time          | `U64` | s | |
| 30543 | Feed-in time          | `U32` | s | |
| 30599 | Number of grid connections               | `U32` | !!! | |
| 31405 | Current spec. active power limitation P  | `U32` | !!! | |
| 30975 | Intermediate circuit voltage             | `S32` | V | |
| 30953 | Internal temperature  | `S32` | °C | |
| 34113 | Internal temperature  | `S32` | °C | |
| 30795 | Grid current          | `U32` | !!! | |


**Below if the list of dropped measurments, which ARE recorded on the SMA SB3.6 but are NOT pushed to the database.**

| Address | Name | Data Type | Unit | Notes |
| :------ | :--- | :-------- | :--- | :---- |
| 32405 | Remaining duration until CP restart | `U32` | !!! | |
| 30561 | Number of events for installer | `U32` | !!! | |
| 35381 | Number of events for installer | `U64` | !!! | |
| 30563 | Number of events for service | `U32` | !!! | |
| 35385 | Number of events for service | `U64` | !!! | |
| 30559 | Number of events for user | `U32` | !!! | |
| 35377 | Number of events for user | `U64` | !!! | |
| 30207 | Nominal power in Fault Mode | `U32` | W | |
| 30203 | Nominal power in Ok Mode | `U32` | W | |
| 31085 | Nominal power in Ok Mode | `U32` | W | |
| 30205 | Nominal power in Warning Mode | `U32` | W | |
| 31221 | EEI displacement power factor | `S32` | !!! | |
| 31247 | Residual current      | `S32` | A | |
| 30225 | Insulation resistance | `U32` | Ohm | |
| 30521 | Operating time        | `U64` | s | |
| 30541 | Operating time        | `U32` | s | |
| 30525 | Feed-in time          | `U64` | s | |
| 30543 | Feed-in time          | `U32` | s | |
| 30599 | Number of grid connections               | `U32` | !!! | |
| 31405 | Current spec. active power limitation P  | `U32` | !!! | |
| 30975 | Intermediate circuit voltage             | `S32` | V | |
| 30953 | Internal temperature  | `S32` | °C | |
| 34113 | Internal temperature  | `S32` | °C | |
